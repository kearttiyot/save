// ตัวแปรเก็บรายชื่อ
var names = [];

// ฟังก์ชันเพิ่มชื่อ
function addName() {
    var newName = document.getElementById("newName").value;
    if (newName !== "") {
        var newEntry = {
            name: newName,
            index: names.length
        };
        names.push(newEntry);
        updateNameList();
        document.getElementById("newName").value = "";
        saveNamesToFile();
        saveNamesToLocalStorage();
    }
}

// ฟังก์ชันลบชื่อ
function removeName() {
    var index = document.getElementById("removeName").value;
    if (index >= 0 && index < names.length) {
        names.splice(index, 1);
        updateNameList();
        saveNamesToLocalStorage();
    }
}

// ฟังก์ชันอัปเดตรายชื่อที่แสดง
function updateNameList() {
    var nameListElement = document.getElementById("nameList");
    nameListElement.innerHTML = "";

    for (var i = 0; i < names.length; i++) {
        var listItem = document.createElement("li");
        listItem.textContent = names[i].name;
        nameListElement.appendChild(listItem);
    }
}

// ฟังก์ชันบันทึกรายชื่อลงในไฟล์
function saveNamesToFile() {
    var content = names.map(entry => entry.name).join('\n');
    var blob = new Blob([content], { type: 'text/plain' });
    var url = URL.createObjectURL(blob);

    var a = document.createElement('a');
    a.href = url;
    a.download = 'output.txt';
    a.textContent = 'Download names';

    document.body.appendChild(a);
}

// ฟังก์ชันบันทึกรายชื่อลงใน local storage
function saveNamesToLocalStorage() {
    localStorage.setItem('names', JSON.stringify(names));
}

// เมื่อโหลดหน้าเว็บ
window.onload = function () {
    var storedNames = localStorage.getItem('names');
    if (storedNames) {
        names = JSON.parse(storedNames);
        updateNameList();
    }
};
